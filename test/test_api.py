import unittest
from datetime import datetime
from eve import Eve
import os
from api import app
import settings
from base64 import b64encode
from os import environ, path
from pymongo.errors import ServerSelectionTimeoutError
import json
from secrets import token_hex
from eve.methods.get import get_internal, getitem_internal

try:
    load_dotenv(find_dotenv())
except Exception as e:
    pass


class basicEveTest(unittest.TestCase):
    """Basic Eve test class
    It should let a client available and a app.
    Make requests with:

    self.test_client.get('/endpoit')

    """

    def setUp(self):
        dir_path = path.dirname(path.realpath(__file__))
        # self.app = Eve(settings=dir_path+'/../settings.py')
        self.app = app
        self.test_client = self.app.test_client()
        # hash = bytes(environ.get("MONGO_USER") + ':' +
        #              environ.get("MONGO_PASS"), "utf-8")
        self.headers = {'Content-Type': 'application/json'}
        self.valid_user = {
            'email': 'teste@domain.com',
            'name': 'Teste',
            'surname': 'Teste',
            'nickname': 'teste',
            'cpf': '12323434545',
            'phone': '12234562345',
            'auth': {
                'password': '12323434545'
            }
        }
        with app.app_context():
            token = token_hex(256)
            app.data.pymongo().db.users.insert_one(self.valid_user)
            # user = app.data.pymongo().db.users.find_one({'nickname': self.valid_user['nickname']})
            with self.app.test_request_context('user/admin'):
                response, _, _, status, _ = get_internal('user_admin')
            user = response['_items'][0]
            app.data.pymongo().db.tokens.insert_one({
                'origin_id': user['_id'],
                'token': token
            })
            token = str(user['_id']) + token
            self.valid_user['_id'] = user['_id']
            self.valid_user['_etag'] = user['_etag']
            self.headers['Authorization'] = 'Bearer %s' % token

    def tearDown(self):
        with self.app.app_context():
            user = app.data.pymongo().db.users.find_one({'nickname': self.valid_user['nickname']})
            self.app.data.pymongo().db['users'].delete_one({'nickname': self.valid_user['nickname']})
            self.app.data.pymongo().db['tokens'].delete_one({'origin_id': user['_id']})
    
    def get(self, url, headers = None):
        headers = self.headers if headers is None else headers
        try:
            response = self.test_client.get(url, headers=headers)
            return response
        except ServerSelectionTimeoutError as e:
            self.skipTest(str(response.response))

    def post(self, url, data):
        try:
            response = self.test_client.post(url, headers=self.headers, data=data)
            return response
        except ServerSelectionTimeoutError as e:
            self.skipTest(str(response.response))

if __name__ == '__main__':
    unittest.main(verbosity=3)
