FROM python:3.6
RUN mkdir -p /app
COPY requirements.txt /app
WORKDIR /app
RUN pip install -r requirements.txt
ADD . /app
CMD ["python", "api.py"]
ENV WORKERS=2
ENV PORT=8000